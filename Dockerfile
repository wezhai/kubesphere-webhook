FROM python:3.9.6
WORKDIR /app
ADD ./app.py /app
ADD ./requirements.txt /app
RUN pip config set global.index-url https://pypi.doubanio.com/simple \
    && pip install --no-cache-dir -r requirements.txt
CMD ["python", "app.py"]