# KubeSphere企业微信机器人发送告警
请求示例
```
curl --location --request POST 'http://10.55.8.101:5000/alert?key=d80b92b5-c8b2-4f67-90d7-18b9c7fdca59' \
--header 'Content-Type: application/json' \
-d '{
  "receiver": "prometheus",
  "status": "firing",
  "alerts": [
    {
      "status": "firing",
      "labels": {
        "cluster": "default",
        "alertname": "test-policy",
        "deployment": "frontend-platform",
        "workload": "Deployment:frontend-platform",
        "rule_id": "example-business-platform-e3002d02a49f82d8",
        "severity": "critical",
        "workspace": "example",
        "alerttype": "metric",
        "namespace": "example-business-platform",
        "owner_kind": "Deployment"
      },
      "annotations": {
        "kind": "Deployment",
        "message": "Some description",
        "resources": "[\"example-i18n-service\",\"frontend-platform\",\"platform-entry\"]",
        "rule_update_time": "2022-10-08T06:00:53Z",
        "rules": "[{\"_metricType\":\"namespace:$2_unavailable_replicas:ratio{$1}\",\"condition_type\":\">=\",\"thresholds\":\"50\",\"unit\":\"%\"}]",
        "summary": "Deployment \u526f\u672c\u4e0d\u53ef\u7528\u7387 >= 50%",
        "aliasName": "\u6d4b\u8bd5\u7b56\u7565"
      },
      "startsAt": "2022-10-08T06:02:16.510748858Z",
      "endsAt": "0001-01-01T00:00:00Z",
      "generatorURL": "/graph?g0.expr=namespace%3Adeployment_unavailable_replicas%3Aratio%7Bnamespace%3D%22example-business-platform%22%2Cworkload%3D~%22Deployment%3Aexample-i18n-service%7CDeployment%3Afrontend-platform%7CDeployment%3Aplatform-entry%22%7D+%3E%3D+0.5&g0.tab=1",
      "fingerprint": "ebafffc0d88b7ead"
    },
    {
      "status": "firing",
      "labels": {
        "deployment": "example-i18n-service",
        "severity": "critical",
        "workload": "Deployment:example-i18n-service",
        "cluster": "default",
        "alerttype": "metric",
        "namespace": "example-business-platform",
        "owner_kind": "Deployment",
        "rule_id": "example-business-platform-e3002d02a49f82d8",
        "workspace": "example",
        "alertname": "test-policy"
      },
      "annotations": {
        "aliasName": "\u6d4b\u8bd5\u7b56\u7565",
        "kind": "Deployment",
        "message": "Some description",
        "resources": "[\"example-i18n-service\",\"frontend-platform\",\"platform-entry\"]",
        "rule_update_time": "2022-10-08T06:00:53Z",
        "rules": "[{\"_metricType\":\"namespace:$2_unavailable_replicas:ratio{$1}\",\"condition_type\":\">=\",\"thresholds\":\"50\",\"unit\":\"%\"}]",
        "summary": "Deployment \u526f\u672c\u4e0d\u53ef\u7528\u7387 >= 50%"
      },
      "startsAt": "2022-10-08T06:02:16.510748858Z",
      "endsAt": "0001-01-01T00:00:00Z",
      "generatorURL": "/graph?g0.expr=namespace%3Adeployment_unavailable_replicas%3Aratio%7Bnamespace%3D%22example-business-platform%22%2Cworkload%3D~%22Deployment%3Aexample-i18n-service%7CDeployment%3Afrontend-platform%7CDeployment%3Aplatform-entry%22%7D+%3E%3D+0.5&g0.tab=1",
      "fingerprint": "5fa71ff516a90ebf"
    },
    {
      "status": "firing",
      "labels": {
        "namespace": "example-business-platform",
        "workspace": "example",
        "severity": "critical",
        "workload": "Deployment:platform-entry",
        "cluster": "default",
        "alertname": "test-policy",
        "alerttype": "metric",
        "deployment": "platform-entry",
        "owner_kind": "Deployment",
        "rule_id": "example-business-platform-e3002d02a49f82d8"
      },
      "annotations": {
        "resources": "[\"example-i18n-service\",\"frontend-platform\",\"platform-entry\"]",
        "rule_update_time": "2022-10-08T06:00:53Z",
        "rules": "[{\"_metricType\":\"namespace:$2_unavailable_replicas:ratio{$1}\",\"condition_type\":\">=\",\"thresholds\":\"50\",\"unit\":\"%\"}]",
        "summary": "Deployment \u526f\u672c\u4e0d\u53ef\u7528\u7387 >= 50%",
        "aliasName": "\u6d4b\u8bd5\u7b56\u7565",
        "kind": "Deployment",
        "message": "Some description"
      },
      "startsAt": "2022-10-08T06:02:16.510748858Z",
      "endsAt": "0001-01-01T00:00:00Z",
      "generatorURL": "/graph?g0.expr=namespace%3Adeployment_unavailable_replicas%3Aratio%7Bnamespace%3D%22example-business-platform%22%2Cworkload%3D~%22Deployment%3Aexample-i18n-service%7CDeployment%3Afrontend-platform%7CDeployment%3Aplatform-entry%22%7D+%3E%3D+0.5&g0.tab=1",
      "fingerprint": "5ba44b54a8693963"
    }
  ],
  "groupLabels": {
    "namespace": "example-business-platform",
    "rule_id": "example-business-platform-e3002d02a49f82d8",
    "alertname": "test-policy"
  },
  "commonLabels": {
    "alerttype": "metric",
    "namespace": "example-business-platform",
    "owner_kind": "Deployment",
    "rule_id": "example-business-platform-e3002d02a49f82d8",
    "severity": "critical",
    "workspace": "example",
    "alertname": "test-policy"
  },
  "commonAnnotations": {
    "aliasName": "\u6d4b\u8bd5\u7b56\u7565",
    "kind": "Deployment",
    "message": "Some description",
    "resources": "[\"example-i18n-service\",\"frontend-platform\",\"platform-entry\"]",
    "rule_update_time": "2022-10-08T06:00:53Z",
    "rules": "[{\"_metricType\":\"namespace:$2_unavailable_replicas:ratio{$1}\",\"condition_type\":\">=\",\"thresholds\":\"50\",\"unit\":\"%\"}]",
    "summary": "Deployment \u526f\u672c\u4e0d\u53ef\u7528\u7387 >= 50%"
  },
  "externalURL": "http://alertmanager-main-2:9093"
}'
```

![image](./images/alert.jpg)
