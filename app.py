import os
import json
import requests
from flask import Flask
from flask import request, jsonify

app = Flask(__name__)

kubeshpere_url = os.getenv("KUBESPHERE_URL") or "http://127.0.0.1:30880"
base_url = "https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key="

levelMap = {
    "critical": "危险告警",
    "error": "重要告警",
    "warning": "一般告警"
}


def send_message(key: str, data: dict):
    url = base_url + key
    content = str()
    content += '您有<font color="warning">**%s条**</font>告警需要关注\n' % len(
        data["告警对象"])
    for k, v in data.items():
        content += ">%s: %s \n" % (k, v)
    content += "[点击查看告警详情](%s/%s/clusters/default/projects/%s/alerts)" % (
        kubeshpere_url, data["企业空间"], data["项目名称"])
    payload = {
        "msgtype": "markdown",
        "markdown": {
            "content": content
        }
    }
    resp = requests.post(url, data=json.dumps(payload))
    if resp.status_code == 200 and '"errmsg":"ok"' in resp.text:
        return True
    return False


@app.route("/alert", methods=["POST"])
def index():
    args = dict(request.args)
    if "key" not in args.keys() or args["key"] == "":
        return jsonify({"message": "发送失败，必须传入企业机器人Key"})
    key = args["key"]
    data = json.loads(request.get_data())
    print(json.dumps(data, indent=2))
    if "kind" not in data["commonAnnotations"]:
        return jsonify({"message": "测试告警接收成功"})

    payload = {}
    obj = list()

    payload = {
        "企业空间": data["commonLabels"]["workspace"],
        "项目名称": data["commonLabels"]["namespace"],
        "告警级别": levelMap.get(data["commonLabels"]["severity"]),
        "策略名称": data["commonLabels"]["alertname"],
        "策略别名": data["commonAnnotations"]["aliasName"],
        "策略描述": data["commonAnnotations"]["message"],
        "资源类型": data["commonAnnotations"]["kind"],
        "消息详情": data["commonAnnotations"]["summary"],
    }

    for i in data["alerts"]:
        obj.append(i["labels"][payload["资源类型"].lower()])

    payload["告警对象"] = obj

    if send_message(key, data=payload):
        return jsonify({"message": "发送成功"})
    return jsonify({"message": "发送失败"})


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
